import threading
from numpy import random
from time import sleep


def producer():
    prodNumber = random.randint(100)
    buffer[inputIndex] = prodNumber


def consumer():
    buffer[outputIndex] = None


def main():
    bufferSize = 5
    global buffer
    buffer = [None] * bufferSize
    global inputIndex
    inputIndex = 0
    global outputIndex
    outputIndex = 0
    # creating thread

    while (True):
        if outputIndex == bufferSize:
            outputIndex = 0
        if inputIndex == bufferSize:
            inputIndex = 0
        consumerThread = threading.Thread(target=consumer)
        producerThread = threading.Thread(target=producer)

        # if not producerThread.is_alive() and not consumerThread.is_alive():
        producerThread.start()

        # starting thread 1
        print('thread 1 before start')
        print('thread 1 start')

        # wait until thread 1 is completely executed
        print('waiting for the thread to finish')

        sleep(1)
        inputIndex += 1

        if inputIndex == outputIndex:
            print("buffer is full! waiting...")
            producerThread.join()
        print(buffer)
        print('thread 1 finished')
        # starting thread 2
        print('thread 2 start')
        # if not producerThread.is_alive() and not consumerThread.is_alive():
        consumerThread.start()
        # wait until thread 2 is completely executed
        sleep(1)
        outputIndex += 1

        if (inputIndex + 1) % bufferSize == outputIndex:
            print("buffer is empty waiting...")
            consumerThread.join()
        print(buffer)

        # both threads completely executed
        print("Done!")



if __name__ == '__main__':
    main()
